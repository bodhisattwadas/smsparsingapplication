package com.example.bodhisattwa.smsparsingapplication.DBPackage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class MyDatabase extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "smsParsingDatabase";
	private static final int DATABASE_VERSION = 1;
	private static final String TABLE_SMS = "smstable";
	
	private static final String SMS_ID = "id";
	private static final String SMS_SENDER = "sender";
	private static final String SMS_TEXT = "sms";
	private static final String SMS_TIMESTAMP = "timestamp";

	

	public MyDatabase(Context context, String name, CursorFactory factory,
					  int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}
	public MyDatabase(Context context){
		super(context, DATABASE_NAME,	null,	DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String createSMSTableParser = "CREATE TABLE "+TABLE_SMS+" ( "+ SMS_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
																	  SMS_SENDER+" TEXT,"+
																	  SMS_TEXT+" TEXT,"+
																	  SMS_TIMESTAMP+" TEXT );";

		db.execSQL(createSMSTableParser);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		arg0.execSQL("DROP TABLE IF EXISTS " + TABLE_SMS+";");
		onCreate(arg0);
		arg0.close();
		
	}
	
	public void insertSMSInDatabase(SMSPopulateClass sms) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(SMS_SENDER, sms.getSmsSender());
		cv.put(SMS_TEXT, sms.getSmsText());
		cv.put(SMS_TIMESTAMP, sms.getSmsTimeStamp());
		db.insert(TABLE_SMS, null, cv);
		db.close();
	}
	public void deleteSMSInDatabase(String timeStamp) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_SMS, SMS_TIMESTAMP + " = ?", new String[] { timeStamp });
		db.close();
	}


	
	public ArrayList<SMSPopulateClass> getSMS() {
		ArrayList<SMSPopulateClass> arrayList = new ArrayList<SMSPopulateClass>();
		SQLiteDatabase db = this.getWritableDatabase();
		String Query= "SELECT * FROM "+TABLE_SMS+";";
		Cursor cursor = db.rawQuery(Query,  null);
		if (cursor.moveToFirst()){
			cursor.moveToFirst();
			do{
				arrayList.add(new SMSPopulateClass(cursor.getString(1), cursor.getString(2), cursor.getString(3)));
			}while(cursor.moveToNext());
		}
		db.close();
		return arrayList;
	}/**
	public void deleteVoucher(String url){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_VOUCHER, VOUCHER_URL+" = '"+url+"'", null);
		db.close();
	}
    */

}
