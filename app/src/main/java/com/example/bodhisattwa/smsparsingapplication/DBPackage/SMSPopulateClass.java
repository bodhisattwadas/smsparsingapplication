package com.example.bodhisattwa.smsparsingapplication.DBPackage;

/**
 * Created by bodhisattwa on 8/20/2016.
 */
public class SMSPopulateClass {
    private String smsSender,smsTimeStamp,smsText;

    public SMSPopulateClass(String smsSender, String smsText, String smsTimeStamp) {
        this.smsSender = smsSender;
        this.smsTimeStamp = smsTimeStamp;
        this.smsText = smsText;
    }

    public String getSmsSender() {
        return smsSender;
    }

    public void setSmsSender(String smsSender) {
        this.smsSender = smsSender;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public String getSmsTimeStamp() {
        return smsTimeStamp;
    }

    public void setSmsTimeStamp(String smsTimeStamp) {
        this.smsTimeStamp = smsTimeStamp;
    }
}
