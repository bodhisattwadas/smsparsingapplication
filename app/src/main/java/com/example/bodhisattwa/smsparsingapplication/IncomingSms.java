package com.example.bodhisattwa.smsparsingapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.example.bodhisattwa.smsparsingapplication.DBPackage.MyDatabase;
import com.example.bodhisattwa.smsparsingapplication.DBPackage.SMSPopulateClass;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bodhisattwa on 8/14/2016.
 */

public class IncomingSms extends BroadcastReceiver{
    final SmsManager sms = SmsManager.getDefault();
    private MyDatabase database;
    private final List<NameValuePair> params = new ArrayList<NameValuePair>();
    private JSONObject jsonObject;
    SharedPreferences sharedpreferences;
    private String urlString;

    //private ProgressDialog pd;

    public IncomingSms() {
        super();
    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        sharedpreferences = context.getSharedPreferences(MainActivity.MyPREFERENCES, context.MODE_PRIVATE);
        urlString = sharedpreferences.getString(MainActivity.applicationURL,new MyString().getUrlToLogin());
        database = new MyDatabase(context);
        final Bundle bundle = intent.getExtras();
        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                if(sharedpreferences.getInt(MainActivity.applicationStatus,2) == 1){
                    for (int i = 0; i < pdusObj.length; i++) {
                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                        String senderNum = phoneNumber;
                        String message = currentMessage.getDisplayMessageBody();
                        String time = Long.toString((long)(System.currentTimeMillis() / 1000L));
                        Log.i("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);

                        if (AppStatus.getInstance(context).isOnline()) {
                            new PostLoginData().execute(senderNum,message,time);
                            database.insertSMSInDatabase(new SMSPopulateClass(senderNum,message,time));
                        } else {
                            database.insertSMSInDatabase(new SMSPopulateClass(senderNum,message,time));
                        }
                        //new PostLoginData().execute(senderNum,message);
                    } // end for loop
                }


            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }
    private class PostLoginData extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... arg0) {
            JSONParser jsonParser = new JSONParser();
            params.add(new BasicNameValuePair("insertSMS", ""));
            params.add(new BasicNameValuePair("senderNumber", arg0[0]));
            params.add(new BasicNameValuePair("smsText", arg0[1]));
            params.add(new BasicNameValuePair("smsTime", arg0[2]));
            try{
                jsonObject = jsonParser.makeHttpRequest(new MyString().getUrlToLogin(),"POST",params);
            }catch(Exception e){
                Log.e("Exception : ","Error on doINBG execute");
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            try {
                String getMessage = jsonObject.getString("Message");
                Log.e("Message from server : ",getMessage);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.e("Exception : ","Error on POST execute");
            }
        }

    }
}
