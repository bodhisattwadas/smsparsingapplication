package com.example.bodhisattwa.smsparsingapplication;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {
    //private MyDatabase database;
    private ToggleButton toggleButton;
    private Button button;
    private EditText editText;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String applicationStatus = "statusKey";
    public static final String applicationStatus2 = "statusKey2";
    public static final String applicationURL = "urlKey";
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toggleButton = (ToggleButton)findViewById(R.id.toggleButton);
        button = (Button)findViewById(R.id.button);
        editText = (EditText)findViewById(R.id.editText);
        editText.setText(new MyString().getUrlToLogin());

        sharedpreferences = getSharedPreferences(MyPREFERENCES, this.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        //Toast.makeText(getApplicationContext(), Integer.toString(sharedpreferences.getInt(applicationStatus,2)), Toast.LENGTH_SHORT).show();

        //Toast.makeText(getApplicationContext(), Integer.toString(sharedpreferences.getInt(applicationStatus2,2)), Toast.LENGTH_SHORT).show();

        if(sharedpreferences.getInt(applicationStatus,2) == 2){
            //Not set yet --> first run
            editor.putInt(applicationStatus,1);
            editor.commit();
            //checkBox.setChecked(true);
            toggleButton.setChecked(true);
        }else if(sharedpreferences.getInt(applicationStatus,2) == 1){
            //Not set yet
            //checkBox.setChecked(true);
            toggleButton.setChecked(true);
        }else if(sharedpreferences.getInt(applicationStatus,2) == 0){
            //checkBox.setChecked(false);
            toggleButton.setChecked(false);
        }
        //editor.putString(applicationURL, new MyString().getUrlToLogin());
        //editor.putInt(applicationStatus,1);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(toggleButton.isChecked()){
                        //editor.putString(applicationURL, editText.getText().toString());
                        editor.putInt(applicationStatus,1);
                        editor.commit();
                        Toast.makeText(getApplicationContext(),"Saved !!",Toast.LENGTH_SHORT).show();
                    }else{
                        //editor.putString(applicationURL, editText.getText().toString());
                        editor.putInt(applicationStatus,0);
                        editor.commit();
                        Toast.makeText(getApplicationContext(),"Saved !!",Toast.LENGTH_SHORT).show();
                    }
            }
        });






        /**
        database = new MyDatabase(getApplicationContext());
        ArrayList<SMSPopulateClass> smsPopulateClasses = database.getSMS();
        for(SMSPopulateClass elem : smsPopulateClasses){
            Log.i(elem.getSmsSender(),elem.getSmsText()+" : "+elem.getSmsTimeStamp());
        }
         */
    }


}
