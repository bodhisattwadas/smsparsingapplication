package com.example.bodhisattwa.smsparsingapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.example.bodhisattwa.smsparsingapplication.DBPackage.MyDatabase;
import com.example.bodhisattwa.smsparsingapplication.DBPackage.SMSPopulateClass;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bodhisattwa on 8/21/2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    private final List<NameValuePair> params = new ArrayList<NameValuePair>();
    private JSONObject jsonObject;
    private MyDatabase database;
    private SharedPreferences sharedpreferences;
    public NetworkChangeReceiver() {
        super();
    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        sharedpreferences = context.getSharedPreferences(MainActivity.MyPREFERENCES, context.MODE_PRIVATE);
        if(checkInternet(context))
        {
            database = new MyDatabase(context);
            ArrayList<SMSPopulateClass> smsPopulateClasses = database.getSMS();
            if(sharedpreferences.getInt(MainActivity.applicationStatus,0) == 1) {
                for (SMSPopulateClass elem : smsPopulateClasses) {
                    new PostSMSTOWeb().execute(elem.getSmsSender(), elem.getSmsText(), elem.getSmsTimeStamp());
                }
            }
        }
        else
        {
            //Toast.makeText(context, "Network Available Do operations", Toast.LENGTH_LONG).show();
            Log.e("Network state : ","Not Available");
        }
    }
    boolean checkInternet(Context context) {
        ServiceManager serviceManager = new ServiceManager(context);
        if (serviceManager.isNetworkAvailable()) {
            return true;
        } else {
            return false;
        }
    }
    private class PostSMSTOWeb extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... arg0) {
            JSONParser jsonParser = new JSONParser();
            params.add(new BasicNameValuePair("insertSMS", ""));
            params.add(new BasicNameValuePair("senderNumber", arg0[0]));
            params.add(new BasicNameValuePair("smsText", arg0[1]));
            params.add(new BasicNameValuePair("smsTime", arg0[2]));
            try{
                jsonObject = jsonParser.makeHttpRequest(new MyString().getUrlToLogin(),"POST",params);
            }catch(Exception e){
                Log.e("Exception : ","Error on doINBG execute");
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            try {
                String getMessage = jsonObject.getString("Message");
                Log.e("Message from server : ",getMessage);
                database.deleteSMSInDatabase(getMessage);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.e("Exception : ","Error on POST execute");
            }
        }

    }
}
